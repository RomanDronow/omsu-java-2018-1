package task3_collections;

import org.junit.Test;
import task1_basics.trainee.Trainee;
import task1_basics.trainee.TraineeException;
import task3_collections.meta.MetaStringException;

import java.util.Arrays;
import java.util.Comparator;

public class GroupSortTest {
    @Test
    public void testSortByGrade() throws TraineeException, MetaStringException {
        Trainee[] trs = new Trainee[6];
        trs[0] = new Trainee("Igor", "Gorbunov", 5);
        trs[1] = new Trainee("Artur", "Buss", 4);
        trs[2] = new Trainee("Roman", "Dronov", 1);
        trs[3] = new Trainee("Tanya", "Fokina", 5);
        trs[4] = new Trainee("Alexandra", "Gridina", 3);
        trs[5] = new Trainee("Someone", "Bad", 2);
        Group group = new Group("A", trs);
        Arrays.sort(group.getStudents(),Comparator.comparingInt(Trainee::getGrade));
        for (Trainee t: group.getStudents()) {
            System.out.println(t);
        }
    }

    @Test
    public void testSortByFirstName() throws TraineeException, MetaStringException {
        Trainee[] trs = new Trainee[6];
        trs[0] = new Trainee("Igor", "Gorbunov", 5);
        trs[1] = new Trainee("Artur", "Buss", 4);
        trs[2] = new Trainee("Roman", "Dronov", 1);
        trs[3] = new Trainee("Tanya", "Fokina", 5);
        trs[4] = new Trainee("Alexandra", "Gridina", 3);
        trs[5] = new Trainee("Someone", "Bad", 2);
        Group group = new Group("A", trs);
        Arrays.sort(group.getStudents());
        for (Trainee t: group.getStudents()) {
            System.out.println(t);
        }
    }
}
