package task3_collections;

import org.junit.Test;
import task1_basics.trainee.Trainee;
import task1_basics.trainee.TraineeException;
import task3_collections.meta.MetaStringException;

import java.util.*;

import static org.junit.Assert.*;
import static task3_collections.TraineeCollections.*;

public class TraineeCollectionsTest {
    @Test
    public void testArrayList() throws TraineeException {
        List<Trainee> trs = new ArrayList<Trainee>();
        trs.add(new Trainee("Alex", "Petrov", 2));
        trs.add(new Trainee("Oleg", "Ivanov", 3));
        trs.add(new Trainee("Dima", "Golubkov", 1));
        trs.add(new Trainee("Tanya", "Pupkina", 5));
        trs.add(new Trainee("Igor", "Mashina", 4));

        System.out.println("Default order:\n");
        System.out.println(trs);

        System.out.println("\nReverted order:\n");
        Collections.reverse(trs);
        System.out.println(trs);

        System.out.println("\nMoved in cycle with step 2:\n");
        Collections.rotate(trs, 2);
        System.out.println(trs);

        System.out.printf("\nTrainee with max. grade: %s", findMaxGrade(trs));

        System.out.println("\n\nSorted by grade:\n");
        Collections.sort(trs, Comparator.comparingInt(Trainee::getGrade));
        System.out.println(trs);

        System.out.println("\nSorted by name:\n");
        Collections.sort(trs);
        System.out.println(trs);

        System.out.printf("\nFind Igor: %s%s", findByName(trs, "Igor"), "\n");
    }

    @Test
    public void testLinkedList() throws TraineeException {
        List<Trainee> trs = new LinkedList<Trainee>();
        trs.add(new Trainee("Alex", "Petrov", 2));
        trs.add(new Trainee("Oleg", "Ivanov", 3));
        trs.add(new Trainee("Dima", "Golubkov", 1));
        trs.add(new Trainee("Tanya", "Pupkina", 5));
        trs.add(new Trainee("Igor", "Mashina", 4));

        System.out.println("Default order:\n");
        System.out.println(trs);

        System.out.println("\nReverted order:\n");
        Collections.reverse(trs);
        System.out.println(trs);

        System.out.println("\nMoved in cycle with step 2:\n");
        Collections.rotate(trs, 2);
        System.out.println(trs);

        System.out.printf("\nTrainee with max. grade: %s\n", findMaxGrade(trs));

        System.out.println("\nSorted by grade:\n");
        Collections.sort(trs, Comparator.comparingInt(Trainee::getGrade));
        System.out.println(trs);

        System.out.println("\nSorted by name:\n");
        Collections.sort(trs);
        System.out.println(trs);

        System.out.printf("\nFind Igor: %s%s", findByName(trs, "Igor"), "\n");
    }


    @Test
    public void testTraineeQueue() throws TraineeException {
        Queue<Trainee> trs = new PriorityQueue<Trainee>();
        trs.add(new Trainee("Alex", "Petrov", 2));
        trs.add(new Trainee("Oleg", "Ivanov", 3));
        trs.add(new Trainee("Dima", "Golubkov", 1));
        trs.add(new Trainee("Tanya", "Pupkina", 5));
        trs.add(new Trainee("Igor", "Mashina", 4));
        assertFalse(isQueueEmpty(trs));
        while (!isQueueEmpty(trs)) {
            System.out.println(trs.poll());
        }
    }

    @Test
    public void testTraineeHashSet() throws TraineeException {
        Set<Trainee> trs = new HashSet<Trainee>();
        trs.add(new Trainee("Alex", "Petrov", 2));
        trs.add(new Trainee("Oleg", "Ivanov", 3));
        trs.add(new Trainee("Dima", "Golubkov", 1));
        trs.add(new Trainee("Tanya", "Pupkina", 5));
        trs.add(new Trainee("Igor", "Mashina", 4));
        Trainee control1 = new Trainee("Oleg", "Ivanov", 3);
        Trainee control2 = new Trainee("Dima", "Ivanov", 5);
        System.out.println(trs);
        assertTrue(trs.contains(control1));
        assertFalse(trs.contains(control2));
    }

    @Test
    public void testTraineeTreeSet() throws TraineeException {
        Set<Trainee> trs = new TreeSet<Trainee>();
        trs.add(new Trainee("Alex", "Petrov", 2));
        trs.add(new Trainee("Oleg", "Ivanov", 3));
        trs.add(new Trainee("Dima", "Golubkov", 1));
        trs.add(new Trainee("Tanya", "Pupkina", 5));
        trs.add(new Trainee("Igor", "Mashina", 4));
        Trainee control1 = new Trainee("Oleg", "Ivanov", 3);
        Trainee control2 = new Trainee("Dima", "Ivanov", 5);
        System.out.println(trs);
        assertTrue(trs.contains(control1));
        assertFalse(trs.contains(control2));
    }

    @Test
    public void testTraineeTreeSet2() throws TraineeException {
        TreeSet<Trainee> trs = new TreeSet<Trainee>();
        trs.add(new Trainee("A", "A", 2));
        trs.add(new Trainee("C", "A", 3));
        trs.add(new Trainee("B", "C", 1));
        trs.add(new Trainee("A", "D", 5));
        trs.add(new Trainee("A", "N", 4));
        trs.add(new Trainee("Z", "Z", 1));
        trs.add(new Trainee("X", "A", 3));

        Trainee control = new Trainee("A", "Control", 2);
        Trainee control2 = new Trainee("D", "C", 1);
        assertTrue(findWithAnotherLastName(trs,control));
        assertFalse(findWithAnotherLastName(trs,control2));
    }


    @Test
    public void testTraineeHashMap() throws MetaStringException, TraineeException {
        Map<Trainee, Institute> map = new HashMap<>();
        map.put(new Trainee("Alex", "Petrov", 2), new Institute("OmSU", "Omsk"));
        map.put(new Trainee("Oleg", "Ivanov", 3), new Institute("OmSU", "Omsk"));
        map.put(new Trainee("Dima", "Golubkov", 1), new Institute("OmSU", "Omsk"));
        map.put(new Trainee("Tanya", "Pupkina", 5), new Institute("Politex", "Omsk"));
        map.put(new Trainee("Igor", "Mashina", 4), new Institute("Politex", "Omsk"));
        map.put(new Trainee("Fedor", "Siniy", 1), new Institute("NSU", "Novosibirsk"));
        map.put(new Trainee("Roma", "Grechka", 3), new Institute("NSU", "Novosibirsk"));

        Trainee control = new Trainee("Alex", "Petrov", 2);
        System.out.println(control + " at " + map.get(control));
        for (Trainee t : map.keySet()) {
            System.out.println(t);
        }
    }

    @Test
    public void testTraineeTreeMap() throws TraineeException, MetaStringException {
        Map<Trainee, Institute> map = new TreeMap<>();
        Institute omsu = new Institute("OmSU", "Omsk");
        Institute osu = new Institute("OSU", "Omsk");
        Institute nsu = new Institute("NSU", "Novosibirsk");

        map.put(new Trainee("Alex", "Petrov", 2), omsu);
        map.put(new Trainee("Oleg", "Ivanov", 3), omsu);
        map.put(new Trainee("Dima", "Golubkov", 1), omsu);
        map.put(new Trainee("Tanya", "Pupkina", 5), osu);
        map.put(new Trainee("Igor", "Mashina", 4), osu);
        map.put(new Trainee("Igor", "Zeleniy", 4), osu);
        map.put(new Trainee("Fedor", "Siniy", 1), nsu);
        map.put(new Trainee("Roma", "Grechka", 3), nsu);

        Trainee control = new Trainee("Alex", "Petrov", 2);
        System.out.println(control + " at " + map.get(control));

        System.out.println();
        for (Trainee t : map.keySet()) {
            System.out.println(t);
        }

        System.out.println();
        for (Trainee t : map.keySet()) {
            System.out.println(t + " at " + map.get(t));
        }


        //// 3-18 from this line ////

        Trainee control2 = new Trainee("Igor", "Rakitin", 2);
        map.put(control2, osu);
        for (Trainee t : map.keySet()) {
            if (t.getFirstName().equals(control2.getFirstName()) && !(t.getLastName().equals(control2.getLastName()))) {
                System.out.println("\n" + control2.getFirstName() + ", but not " + control2.getLastName() + ": " + t + " at " + map.get(t));
            }
        }
    }
}
