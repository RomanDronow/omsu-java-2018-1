package task3_collections;

import org.junit.Test;
import task1_basics.trainee.Trainee;
import task1_basics.trainee.TraineeException;
import task3_collections.meta.MetaStringException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GroupTest {

    @Test
    public void testGroup() throws TraineeException, MetaStringException {
        Group group1 = new Group("MPB604",
                new Trainee("Tanya", "Fokina", 5),
                new Trainee("Igor", "Gorbunov", 5));

        Group group2 = new Group("HHB503", new Trainee("Darya", "Veber", 5));

        Group group1_copy = new Group(group1);

        assertEquals(group1, group1_copy);
        assertEquals("MPB604", group1.getTitle());
        assertNotEquals(group1, group2);
    }

    @Test(expected = MetaStringException.class)
    public void testTitleException() throws MetaStringException {
        Group group = new Group("", new Trainee());
    }

    @Test(expected = MetaStringException.class)
    public void testStudentsException() throws MetaStringException {
        Trainee[] trs = new Trainee[2];
        Group group = new Group("A", trs);
    }

    @Test(expected = MetaStringException.class)
    public void testStudentsException2() throws MetaStringException {
        Trainee[] trs = new Trainee[0];
        Group group = new Group("A", trs);
    }
}
