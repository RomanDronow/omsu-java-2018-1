package task3_collections;

import org.junit.Test;

import java.util.*;

public class CollectionsTest {
    @Test
    public void testRandomAccessLists() {
        List<Integer> arlist = new ArrayList<Integer>();
        List<Integer> linlist = new LinkedList<Integer>();

        for (int i = 0; i < 100000; i++) {
            arlist.add(i);
            linlist.add(i);
        }

        Random random = new Random();

        double arStartTime = (double) System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            int arres = arlist.get(random.nextInt(100000));
        }
        double arFinistTime = (double) System.nanoTime();
        System.out.println((arFinistTime - arStartTime) / 1e6 + " ms");

        double linStartTime = (double) System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            int linres = linlist.get(random.nextInt(100000));
        }
        double linFinishTime = (double) System.nanoTime();
        System.out.println((linFinishTime - linStartTime) / 1e6 + " ms");
    }

    @Test
    public void testAccesstoListAndSet() {
        List<Integer> arlist = new ArrayList<Integer>();
        Set<Integer> hashset = new HashSet<Integer>();
        Set<Integer> treeset = new TreeSet<Integer>();
        Random random = new Random();

        for (int i = 0; i < 100000; i++) {
            arlist.add(random.nextInt(100000));
        }

        while (hashset.size() != 100000) {
            hashset.add(random.nextInt(100000));
        }
        while (treeset.size() != 100000) {
            treeset.add(random.nextInt(100000));
        }

        double arinit = (double) System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            boolean b = arlist.contains(i);
        }
        double arfinish = (double) System.nanoTime();
        System.out.println((arfinish - arinit) / 1e6 + " ms");


        double hashinit = (double) System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            boolean b = hashset.contains(i);
        }
        double hashfinish = (double) System.nanoTime();
        System.out.println((hashfinish - hashinit) / 1e6 + " ms");

        double treeinit = (double) System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            boolean b = treeset.contains(i);
        }
        double treefinish = (double) System.nanoTime();
        System.out.println((treefinish - treeinit) / 1e6 + " ms");
    }

    @Test
    public void testBitSet() {
        BitSet bs = new BitSet();
        bs.set(5);
        bs.set(12);
        bs.set(14);
        System.out.println(bs);
        int control = 12;
        System.out.println("Is " + control + " in bitset? " + bs.get(control));
        control = 13;
        System.out.println("Is " + control + " in bitset? " + bs.get(control));
        bs.clear(5, 12);
        System.out.println(bs);
    }

    @Test
    public void testSpeedOfDifferentSets() {
        BitSet bs = new BitSet();
        Set<Integer> hs = new HashSet<>();
        Set<Integer> ts = new TreeSet<>();

        double bsStartTime = (double) System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            bs.set(i);
        }
        double bsFinishTime = (double) System.nanoTime();

        System.out.println("For BitSet time is: " + (bsFinishTime - bsStartTime) / 1e6 + " ms");

        double hsStartTime = (double) System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            hs.add(i);
        }
        double hsFinishTime = (double) System.nanoTime();

        System.out.println("For HashSet time is: " + (hsFinishTime - hsStartTime) / 1e6 + " ms");

        double tsStartTime = (double) System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            ts.add(i);
        }
        double tsFinishTime = (double) System.nanoTime();

        System.out.println("For TreeSet time is: " + (tsFinishTime - tsStartTime) / 1e6 + " ms");
    }

    @Test
    public void testEnumSet() {
        EnumSet<Colour> setNone = EnumSet.noneOf(Colour.class);
        EnumSet<Colour> setAll = EnumSet.allOf(Colour.class);
        EnumSet<Colour> setRed = EnumSet.of(Colour.RED);
        EnumSet<Colour> setRedToBlue = EnumSet.range(Colour.RED, Colour.BLUE);
        System.out.println(setNone);
        System.out.println(setAll);
        System.out.println(setRed);
        System.out.println(setRedToBlue);
        System.out.println();
        System.out.println("Check if BLUE contains in these sets");
        System.out.println("setNone: " + setNone.contains(Colour.BLUE));
        System.out.println("setAll: " + setAll.contains(Colour.BLUE));
        System.out.println("setRed: " + setRed.contains(Colour.BLUE));
        System.out.println("setRedToBlue: " + setRedToBlue.contains(Colour.BLUE));
    }

    @Test
    public void testUniqueRows() {
        int[][] matrix = {{1, 2, 3, 4}, {3, 2, 1, 4}, {5, 1, 2, 4}, {1, 3, 2, 4}};
        System.out.println(MatrixDemo.numberOfUniqueRows(matrix));
    }
}
