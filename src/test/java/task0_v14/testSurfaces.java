package task0_v14;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static task0_v14.SurfaceType.*;

public class testSurfaces {

    @Test
    public void testSurface() {
        Surface s = new Surface();
        Surface s1 = new Surface("Everest", MOUNTAIN);
        Surface s2 = new Surface("Red Sea", SALTY_WATER);

        assertEquals("Default", s.getName());
        assertEquals(PLAIN, s.getType());
        s.setType(FRESH_WATER);
        s.setName("Baikal");
        assertEquals("Baikal", s.getName());
        assertEquals(FRESH_WATER, s.getType());

        assertNotEquals(new Surface("Red Sea", SALTY_WATER), s1);
        assertEquals(new Surface("Red Sea", SALTY_WATER), s2);
        assertEquals("mountain Everest", s1.toString());

        assertEquals(new Surface("Red Sea",SALTY_WATER).hashCode(),s2.hashCode());
    }

    @Test
    public void testSurfaceList() {
        SurfaceList slist = new SurfaceList();
        Surface s = new Surface("Everest", MOUNTAIN);
        Surface s1 = new Surface("Atlantic Ocean", SALTY_WATER);
        slist.addSurface("Titikaka", FRESH_WATER);
        slist.addSurface(s);
        slist.addSurface(s1);
        assertEquals(new Surface("Everest", MOUNTAIN), slist.getSurface(1));
        slist.removeSurface(1);
        assertEquals(new Surface("Atlantic Ocean", SALTY_WATER), slist.getSurface(1));
    }
}
