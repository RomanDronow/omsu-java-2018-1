package task5_java8features;

import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class LambdaTest {
    @Test
    public void testStringSplitThenCount() {
        String s = "Lorem ipsum dolor sit amet";
        List<String> strings = Lambda.split.apply(s);
        System.out.println(strings);
        int r = Lambda.count.apply(strings);
        assertEquals(r, 5);
    }

    @Test
    public void testStringSplitAndCount() {
        String s = "Lorem ipsum dolor sit amet";
        int r = Lambda.splitAndCount.apply(s);
        assertEquals(r, 5);
    }

    @Test
    public void testStringSplitAndCountCompose() {
        String s = "Lorem ipsum dolor sit amet";
        int r = Lambda.splitAndCountCompose.apply(s);
        assertEquals(r, 5);
    }

    @Test
    public void testCreatePerson() {
        Person p = Lambda.create.apply("Test");
        assertEquals("Test", p.getName());
    }

    @Test
    public void testBiFunctionMax() {
        int c = Lambda.max.apply(12, 3);
        assertEquals(12, c);
    }

    @Test
    public void testSupplierDate() {
        assertNotNull(Lambda.getCurrentDate);
    }

    @Test
    public void testIsEven() {
        assertTrue(Lambda.isEven.test(2));
        assertFalse(Lambda.isEven.test(3));
    }

    @Test
    public void testAreEqual() {
        assertTrue(Lambda.areEqual.test(4, 4));
        assertFalse(Lambda.areEqual.test(2, 4));
    }

    @Test
    public void testIntStream() {
        IntStream intStream = Lambda.transform(IntStream.of(1, 2, 3, 4, 5), x -> x * x);
        IntStream test = IntStream.of(1, 4, 9, 16, 25);
        assertEquals(Arrays.toString(test.toArray()), Arrays.toString(intStream.toArray()));
    }

    @Test
    public void sumTest() {
        List<Integer> list = new ArrayList<>();
        Collections.addAll(list,
                1, 2, 3, 4, 5, 6);

        assertEquals(21, Lambda.sum(list));
    }

    @Test
    public void productTest() {
        List<Integer> list = new ArrayList<>();
        Collections.addAll(list,
                1, 2, 3, 4, 5, 6);

        assertEquals(720, Lambda.product(list));
    }

    @Test
    public void getListOfSortedUniqueNamesPersonsOlderThirtyYearsTest(){
        List<Person> listOfPerson = new ArrayList<>();
        Collections.addAll(listOfPerson,
                new Person("p1", 31),
                new Person("p2", 29),
                new Person("p1", 32),
                new Person("pp1", 234),
                new Person("ppp1", 54),
                new Person("pp1p", 2)
        );

        List<String> sortedListOfPerson = new ArrayList<>();
        Collections.addAll(sortedListOfPerson,
                "p1", "pp1", "ppp1"
        );

        assertEquals(sortedListOfPerson, Lambda.uniqueNamesBelowThirty.apply(listOfPerson));
    }

    @Test
    public void getListOfSortedUniqueNamesPersonsOlderThirtyYearsTest2(){
        List<Person> listOfPerson = new ArrayList<>();
        Collections.addAll(listOfPerson,
                new Person("p1", 31),
                new Person("p1", 32),
                new Person("pp1", 234),
                new Person("ppp1", 54),
                new Person("ppp1", 56),
                new Person("ppp1", 32)
        );

        List<String> sortedListOfPerson = new ArrayList<>();
        Collections.addAll(sortedListOfPerson,
                "pp1", "p1", "ppp1"
        );

        assertEquals(sortedListOfPerson, Lambda.uniqueNamesBelowThirtyGrouped.apply(listOfPerson));
    }
}
