package task1_basics.trainee;

import java.io.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TraineeIOTest {
    @Test
    public void testTraineeInTxt() throws TraineeException, IOException {           // Записать и рочитать экземпляр Trainee из этого файла(несколько строк).
        Trainee tr1 = new Trainee("Roman", "Dronov", 5);
        TraineeIO.write(tr1, new FileWriter("resources/trainee.txt"));
        Trainee tr2 = TraineeIO.read(new FileReader("resources/trainee.txt"));
        assertEquals(tr1, tr2);
    }

    @Test
    public void testTraineeInTxtOneLine() throws TraineeException, IOException {            // Записать и прочитать экземпляр Trainee из этого файла(одной строкой).
        Trainee tr1 = new Trainee("Roma", "Dronov", 5);
        TraineeIO.writeAsLine(tr1, new FileWriter("resources/trainee-1-line.txt"));
        Trainee tr2 = TraineeIO.readAsLine(new FileReader("resources/trainee-1-line.txt"));
        assertEquals(tr1, tr2);
    }

    @Test
    public void testSerializeTrainee() throws TraineeException, IOException {
        Trainee tr1 = new Trainee("somebody", "else", 4);
        ObjectOutputStream oos;
        try (FileOutputStream fos = new FileOutputStream("resources/trainee.dat")) {
            oos = new ObjectOutputStream(fos);
            oos.writeObject(tr1);
        }
    }

    @Test
    public void testDeserializeTrainee() throws IOException, TraineeException, ClassNotFoundException {
        ObjectInputStream ois;
        try (FileInputStream fis = new FileInputStream("resources/trainee.dat")) {
            ois = new ObjectInputStream(fis);
            Trainee tr2 = (Trainee) ois.readObject();
            assertEquals(new Trainee("somebody", "else", 4), tr2);
        }
    }

    @Test
    public void testByteArrayTrainee() throws IOException, TraineeException, ClassNotFoundException {
        Trainee tr1 = new Trainee("same", "person", 2);
        ObjectInputStream ois;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(tr1);
            ois = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
            Trainee tr2 = (Trainee) ois.readObject();
            assertEquals(tr1, tr2);
        }
    }

    @Test
    public void testGsonTrainee() throws TraineeException {
        Trainee trainee = new Trainee("Alexeo", "Bajdakov", 4);
        String json = TraineeIO.serializeToGson(trainee);
        Trainee trainee1 = TraineeIO.deserializeFromGson(json);
        System.out.println(trainee1);
    }

    @Test
    public void testGsonTraineeInFile() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Alexeo", "Bajdakov", 4);
        TraineeIO.serializeToGsonInFile(trainee, "resources/trainee.json");
        Trainee trainee1 = TraineeIO.deserializeWithGsonFromFile("resources/trainee.json");
        System.out.println(trainee1);
    }
}

