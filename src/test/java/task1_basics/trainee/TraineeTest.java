package task1_basics.trainee;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TraineeTest {
    @Test
    public void testTrainee() throws TraineeException {
        Trainee tr1 = new Trainee("Tanya", "Fokina", 5);
        Trainee tr1_copy = new Trainee(tr1);
        assertEquals("Tanya", tr1.getFirstName());
        assertEquals("Fokina", tr1.getLastName());
        assertEquals(5, tr1.getGrade());

        Trainee tr2 = new Trainee();
        tr1.setFirstName(tr2.getFirstName());
        tr1.setLastName(tr2.getLastName());
        tr1.setGrade(tr2.getGrade());

        assertEquals(tr1, tr2);
        assertNotEquals(tr1, tr1_copy);
        assertEquals(tr1.hashCode(), tr2.hashCode());
    }


    @Test(expected = TraineeException.class)
    public void testGradeException() throws TraineeException {
        Trainee tr1 = new Trainee("Igor", "Gorbunov", -5); //wrong grade
    }

    @Test(expected = TraineeException.class)
    public void testLastNameException() throws TraineeException {
        Trainee tr2 = new Trainee("Artur", null, 4); // empty last name
    }

    @Test(expected = TraineeException.class)
    public void testFirstNameException() throws TraineeException {
        Trainee tr3 = new Trainee("", "Grechka", 3); // empty one name
    }
}
