package task1_basics;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import org.junit.Test;

import static org.junit.Assert.*;

public class FileTest {
    @Test
    public void testFileMethods() throws IOException {
        File folder1 = new File("resources/folder1");
        File folder2 = new File("resources/folder1/folder2/folder3");
        File text = new File("resources/text.txt");

        folder1.mkdir();
        folder2.mkdirs();
        text.createNewFile();

        assertTrue(text.exists());
        assertTrue(folder2.exists());
        assertTrue(folder1.exists());

        folder2.renameTo(new File("resources/folder1/folder2/renamedFolder4"));
        folder1.renameTo(new File("resources/renamedFolder1"));
    }

    @Test
    public void testFileMethods2() throws IOException {
        File folder = new File("resources/sample_folder");
        File[] file = new File[5];
        file[0] = new File("resources/sample_folder/text.txt");
        file[1] = new File("resources/sample_folder/img.jpg");
        file[2] = new File("resources/sample_folder/img.png");
        file[3] = new File("resources/sample_folder/note.txt");
        file[4] = new File("resources/sample_folder/some.dat");

        folder.mkdir();
        for (int i = 0; i < file.length; i++) {
            file[i].createNewFile();
        }
        assertTrue(file[0].isFile());
        assertFalse(file[0].isDirectory());
        assertTrue(folder.isDirectory());
        assertFalse(folder.isFile());

        String[] arrayOfFilesInFolder = folder.list();
        for (String s : arrayOfFilesInFolder) {
            System.out.println(s);
        }

        FilenameFilter imgs = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                return lowercaseName.startsWith("img");
            }
        };
        String[] check = folder.list(imgs);
        System.out.println();
        for (String s : check) {
            System.out.println(s);
        }
    }
}
