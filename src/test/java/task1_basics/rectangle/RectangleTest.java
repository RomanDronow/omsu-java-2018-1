package task1_basics.rectangle;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class RectangleTest {
    @Test
    public void testRectangle() throws RectangleException {
        Rectangle r = new Rectangle(1.0, 1.0, 5.0, 5.0);
        r.setLeftX(2.0);
        r.setLeftY(2.0);
        r.setRightX(3.0);
        r.setRightY(3.0);
        double[] r_match = {2.0, 2.0, 3.0, 3.0};
        double[] coords = {r.getLeftX(), r.getLeftY(), r.getRightX(), r.getRightY()};
        assertArrayEquals(r_match, coords, 0.01);

        Rectangle ro = new Rectangle();
        ro.setLeftTop(-1.0, -1.0);
        ro.setRightBottom(5.0, 5.0);
        double[] ro_match = {-1.0, -1.0, 5.0, 5.0};
        double[] ro_coords = {ro.getLeftX(), ro.getLeftY(), ro.getRightX(), ro.getRightY()};
        assertArrayEquals(ro_match, ro_coords, 0.01);
    }

    @Test(expected = RectangleException.class)
    public void testRectangleException() throws RectangleException {
        Rectangle ro = new Rectangle();
        ro.setLeftTop(11.0, 11.0);
    }
}
