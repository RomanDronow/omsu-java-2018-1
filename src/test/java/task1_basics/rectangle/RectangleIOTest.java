package task1_basics.rectangle;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static task1_basics.rectangle.RectangleIO.*;

public class RectangleIOTest {
    @Test
    public void testRectangleReadWrite() throws IOException, RectangleException, ClassNotFoundException {
        Rectangle r = new Rectangle(4.045, 14.0, 26.033, 63.011);
        FileOutputStream fos = new FileOutputStream("resources/rectangle.dat");
        try (DataOutputStream dos = new DataOutputStream(fos)) {
            writeToFile(dos, r);
        }
        DataInputStream dis;
        try (FileInputStream fis = new FileInputStream("resources/rectangle.dat")) {
            dis = new DataInputStream(fis);
        }
        Rectangle r2 = new Rectangle();
        readFromFile(dis, r2);
        assertEquals(r, r2);
    }

    @Test
    public void testMultipleReadWrite() throws RectangleException, IOException {
        Rectangle[] rectangles = new Rectangle[5];
        rectangles[0] = new Rectangle(11.0, 1.56, 23.45, 1.69);
        rectangles[1] = new Rectangle(1.0, 1.0, 5.0, 5.0);
        rectangles[2] = new Rectangle(21.0, 1.0, 25.0, 5.0);
        rectangles[3] = new Rectangle(1.0, 11.0, 5.0, 15.0);
        rectangles[4] = new Rectangle(12.34, 56.78, 89.10, 91.01);
        writeFiveRectangles(rectangles,"resources/rectangles.dat");
        Rectangle[] rectanglesFromFile = readFiveRectangles("resources/rectangles.dat");
    }
}
