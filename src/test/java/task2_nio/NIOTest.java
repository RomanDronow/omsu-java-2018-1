package task2_nio;

import org.junit.Test;
import task1_basics.trainee.Trainee;
import task1_basics.trainee.TraineeException;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.file.Path;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static task2_nio.NewIO.*;

public class NIOTest {

    @Test
    public void testByteBuffer() throws IOException, TraineeException {
        Trainee tr = new Trainee("Roma", "Dronov", 5);
        Trainee tr2 = readByteBuffer("resources/trainee-1-line.txt");
        assertEquals(tr, tr2);
    }

    @Test
    public void testMappedByteBuffer() throws IOException, TraineeException {
        Trainee tr = new Trainee("Roma", "Dronov", 5);
        Trainee tr2 = readMappedByteBuffer("resources/trainee-1-line.txt");
        assertEquals(tr, tr2);
    }

    @Test
    public void testMappedByteBufferWriteNumbers() throws IOException {
        mappedByteBufferWriteNumbers("resources/numbers.txt");
    }

    @Test
    public void testMappedByteBufferReadNumbers() throws IOException {
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        int[] array2 = readNumbers("resources/numbers.txt");
        assertArrayEquals(array,array2);
    }

    @Test
    public void testWriteReadTraineeToByteBuffer() throws TraineeException, IOException, ClassNotFoundException {
        Trainee tr = new Trainee("Roma", "Dronov", 5);
        ByteBuffer bb = writeTraineeToByteBuffer(tr);
        Trainee tr2 = readTraineeFromByteBuffer(bb);
        assertEquals(tr,tr2);
    }
}