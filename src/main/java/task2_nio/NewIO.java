package task2_nio;

import task1_basics.trainee.Trainee;
import task1_basics.trainee.TraineeException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class NewIO {
    public static Trainee readByteBuffer(String path) throws IOException, TraineeException {

        try (FileChannel fc = FileChannel.open(Paths.get(path), StandardOpenOption.READ)) {
            ByteBuffer bb;
            bb = ByteBuffer.allocate((int) fc.size());
            fc.read(bb);

            String[] params = new String(bb.array(), StandardCharsets.UTF_8).split(" ");
            return new Trainee(params[0], params[1], Integer.parseInt(params[2]));
        }
    }

    public static Trainee readMappedByteBuffer(String path) throws IOException, TraineeException {

        try (FileChannel fc = FileChannel.open(Paths.get(path), StandardOpenOption.READ)) {
            MappedByteBuffer mbb;
            byte[] array;
            mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            array = new byte[(int) fc.size()];

            mbb.get(array);
            String[] params = new String(array, StandardCharsets.UTF_8).split(" ");
            return new Trainee(params[0], params[1], Integer.parseInt(params[2]));
        }
    }

    public static void mappedByteBufferWriteNumbers(String path) throws IOException {
        try (FileChannel fc = FileChannel.open(Paths.get(path), new StandardOpenOption[]{StandardOpenOption.CREATE, StandardOpenOption.READ, StandardOpenOption.WRITE})) {
            MappedByteBuffer mbb;
            mbb = fc.map(FileChannel.MapMode.READ_WRITE, 0, 100 * Integer.BYTES);

            for (int i = 0; i < 100; i++) {
                mbb.putInt(i);
            }
        }
    }

    public static int[] readNumbers(String path) throws IOException {
        try (FileInputStream fis = new FileInputStream(path);) {
            int[] array;
            DataInputStream dis = new DataInputStream(fis);
            array = new int[100];
            for (int i = 0; i < array.length; i++) {
                array[i] = dis.readInt();
            }
            return array;
        }
    }

    public static ByteBuffer writeTraineeToByteBuffer(Trainee tr) throws IOException {
        ObjectOutputStream oos;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(tr);
            return ByteBuffer.wrap(baos.toByteArray());
        }
    }

    public static Trainee readTraineeFromByteBuffer(ByteBuffer bb) throws IOException, TraineeException, ClassNotFoundException {
        ObjectInputStream ois;
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bb.array())) {
            ois = new ObjectInputStream(bais);
        }
        return (Trainee) ois.readObject();
    }
}