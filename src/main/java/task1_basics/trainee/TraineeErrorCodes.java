/* Создать enum TraineeErrorCodes,
добавить в него поле - текстовую строку,
описывающую ошибку и необходимые методы. */

package task1_basics.trainee;

public enum TraineeErrorCodes {
    EMPTY_FIRSTNAME("First name is empty"),
    EMPTY_LASTNAME("Last name is empty"),
    WHITESPACES_IN_NAME("Name contains whitespaces"),
    WRONG_GRADE("Grade must be from 1 to 5");

    private final String errorString;

    TraineeErrorCodes(String s) {
        this.errorString = s;
    }

    public String getErrorString() {
        return errorString;
    }
}
