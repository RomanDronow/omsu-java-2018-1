/*
Создать класс Trainee со следующими полями
      * имя - текстовая строка, не может быть null или пустой строкой
      * фамилия -текстовая строка, не может быть null или пустой строкой
      * оценка - целое, допустимые значения от 1 до 5.

 */

package task1_basics.trainee;

import java.io.Serializable;

public class Trainee implements Serializable, Comparable<Trainee> {
    private String firstName, lastName;
    private int grade;

    public Trainee() {
        firstName = "Roman";
        lastName = "Dronov";
        grade = 5;
    }

    public Trainee(String firstName, String lastName, int grade) throws TraineeException {
        checkFirstName(firstName);
        checkLastName(lastName);
        checkGrade(grade);
        this.firstName = firstName;
        this.lastName = lastName;
        this.grade = grade;
    }

    public Trainee(Trainee trainee) {
        this.firstName = trainee.firstName;
        this.lastName = trainee.lastName;
        this.grade = trainee.grade;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws TraineeException {
        checkFirstName(firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws TraineeException {
        checkLastName(lastName);
        this.lastName = lastName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) throws TraineeException {
        checkGrade(grade);
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainee)) return false;

        Trainee trainee = (Trainee) o;

        if (getGrade() != trainee.getGrade()) return false;
        if (getFirstName() != null ? !getFirstName().equals(trainee.getFirstName()) : trainee.getFirstName() != null)
            return false;
        return getLastName() != null ? getLastName().equals(trainee.getLastName()) : trainee.getLastName() == null;
    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + getGrade();
        return result;
    }

    private static void checkFirstName(String name) throws TraineeException {
        if (name == null || name.length() == 0) throw new TraineeException(TraineeErrorCodes.EMPTY_FIRSTNAME);
        else {
            for (int i = 0; i < name.length(); i++)
                if (name.charAt(i) == ' ') throw new TraineeException(TraineeErrorCodes.WHITESPACES_IN_NAME);
        }
    }

    private static void checkLastName(String name) throws TraineeException {
        if (name == null || name.length() == 0) throw new TraineeException(TraineeErrorCodes.EMPTY_LASTNAME);
        else {
            for (int i = 0; i < name.length(); i++)
                if (name.charAt(i) == ' ') throw new TraineeException(TraineeErrorCodes.WHITESPACES_IN_NAME);
        }
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " " + grade;
    }

    private static void checkGrade(int grade) throws TraineeException {
        if (grade < 1 || grade > 5) {
            throw new TraineeException(TraineeErrorCodes.WRONG_GRADE);
        }
    }

    @Override
    public int compareTo(Trainee t) {
        int result = this.firstName.compareTo(t.getFirstName());
        if(result == 0){
            result = this.lastName.compareTo(t.getLastName());
        }
        return result;
    }
}
