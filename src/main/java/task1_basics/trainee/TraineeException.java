package task1_basics.trainee;

public class TraineeException extends Exception{
    public TraineeException(TraineeErrorCodes error) {
        super(error.getErrorString());
    }
}
