package task1_basics.trainee;

import com.google.gson.Gson;

import java.io.*;

public class TraineeIO {
    public static void write(Trainee t, Writer writer) throws IOException {           // Создать экземпляр Trainee и вывести его в текстовый файл построчно
        try (BufferedWriter bw = new BufferedWriter(writer)) {
            bw.write(t.getFirstName());
            bw.newLine();
            bw.write(t.getLastName());
            bw.newLine();
            bw.write(String.valueOf(t.getGrade()));
            bw.newLine();
        }
    }

    public static Trainee read(Reader reader) throws IOException, TraineeException {
        Trainee t = new Trainee();
        try (BufferedReader br = new BufferedReader(reader)) {
            t.setFirstName(br.readLine());
            t.setLastName(br.readLine());
            t.setGrade(Integer.parseInt(br.readLine()));
        }
        return t;
    }

    public static void writeAsLine(Trainee t, Writer writer) throws IOException {    // Вывести экземпляр Trainee в текстовый файл, записав имя, фамилию и оценку в одну строку файла
        try (BufferedWriter bw = new BufferedWriter(writer)) {
            bw.write(t.getFirstName() + " " + t.getLastName() + " " + String.valueOf(t.getGrade()));
        }
    }

    public static Trainee readAsLine(Reader reader) throws IOException, TraineeException {
        Trainee t = new Trainee();
        try (BufferedReader br = new BufferedReader(reader)) {
            String tmp = br.readLine();
            String[] tmps = tmp.split(" ");
            if (tmps.length == 3) {
                t.setFirstName(tmps[0]);
                t.setLastName(tmps[1]);
                t.setGrade(Integer.parseInt(tmps[2]));
            } else throw new IOException("Wrong syntax");
        }
        return t;
    }

    public static String serializeToGson(Trainee trainee) {
        Gson gson = new Gson();
        return gson.toJson(trainee);
    }

    public static Trainee deserializeFromGson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Trainee.class);
    }

    public static void serializeToGsonInFile(Trainee trainee, String path) throws FileNotFoundException {
        String json = serializeToGson(trainee);
        try (PrintStream ps = new PrintStream(new FileOutputStream(path))) {
            ps.print(json);
        }
    }

    public static Trainee deserializeWithGsonFromFile(String path) throws IOException {
        String json;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            json = br.readLine();
        }
        return deserializeFromGson(json);
    }
}
