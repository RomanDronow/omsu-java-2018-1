package task1_basics.rectangle;

public class RectangleException extends Exception {
    public RectangleException(String message){
        super(message);
    }
}
