package task1_basics.rectangle;

import java.io.*;

public class RectangleIO {
    public static void writeToFile(DataOutputStream dos, Rectangle r) throws IOException {
        dos.writeDouble(r.getLeftX());
        dos.writeDouble(r.getLeftY());
        dos.writeDouble(r.getRightX());
        dos.writeDouble(r.getRightY());
    }

    public static void readFromFile(DataInputStream dis, Rectangle r) throws IOException, RectangleException {
        r.setRightX(Double.MAX_VALUE);
        r.setRightY(Double.MAX_VALUE);
        r.setLeftX(dis.readDouble());
        r.setLeftY(dis.readDouble());
        r.setRightX(dis.readDouble());
        r.setRightY(dis.readDouble());
    }

    public static void readFromFile(RandomAccessFile raf, Rectangle r) throws IOException, RectangleException {
        r.setRightX(Double.MAX_VALUE);
        r.setRightY(Double.MAX_VALUE);
        r.setLeftX(raf.readDouble());
        r.setLeftY(raf.readDouble());
        r.setRightX(raf.readDouble());
        r.setRightY(raf.readDouble());
    }

    public static void writeFiveRectangles(Rectangle[] rectangles, String path) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        try (DataOutputStream dos = new DataOutputStream(fos)) {
            for (Rectangle rectangle : rectangles) {
                writeToFile(dos, rectangle);
            }
        }
    }

    public static Rectangle[] readFiveRectangles(String path) throws IOException, RectangleException {
        Rectangle[] rectanglesFromFile;
        try (RandomAccessFile raf = new RandomAccessFile(path, "r")) {
            try (PrintStream ps = new PrintStream(System.out)) {
                rectanglesFromFile = new Rectangle[5];
                for (int i = 0; i < rectanglesFromFile.length; i++) {
                    rectanglesFromFile[i] = new Rectangle();
                }
                int bytesForRectangle = Double.BYTES * 4;
                for (int i = rectanglesFromFile.length - 1; i >= 0; i--) {
                    raf.seek(i * bytesForRectangle);
                    readFromFile(raf, rectanglesFromFile[i]);
                    ps.println(rectanglesFromFile[i]);
                }
            }
        }
        return rectanglesFromFile;
    }
}
