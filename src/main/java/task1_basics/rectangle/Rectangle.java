package task1_basics.rectangle;

public class Rectangle {
    private double leftX, leftY, rightX, rightY;

    public Rectangle() {
        this.leftX = 0.0;
        this.leftY = 0.0;
        this.rightX = 1.0;
        this.rightY = 1.0;
    }

    public Rectangle(double leftX, double leftY, double rightX, double rightY) throws RectangleException {
        checkAngles(leftX, leftY, rightX, rightY);
        this.leftX = leftX;
        this.leftY = leftY;
        this.rightX = rightX;
        this.rightY = rightY;
    }

    public double getLeftX() {
        return leftX;
    }

    public void setLeftX(double leftX) throws RectangleException {
        checkAngles(leftX, this.leftY, this.rightX, this.rightY);
        this.leftX = leftX;
    }

    public double getLeftY() {
        return leftY;
    }

    public void setLeftY(double leftY) throws RectangleException {
        checkAngles(this.leftX, leftY, this.rightX, this.rightY);
        this.leftY = leftY;
    }

    public double getRightX() {
        return rightX;
    }

    public void setRightX(double rightX) throws RectangleException {
        checkAngles(this.leftX, this.leftY, rightX, this.rightY);
        this.rightX = rightX;
    }

    public double getRightY() {
        return rightY;
    }

    public void setRightY(double rightY) throws RectangleException {
        checkAngles(this.leftX, this.leftY, this.rightX, rightY);
        this.rightY = rightY;
    }

    public void setLeftTop(double leftX, double leftY) throws RectangleException {
        checkAngles(leftX, leftY, this.rightX, this.rightY);
        this.leftX = leftX;
        this.leftY = leftY;
    }

    public void setRightBottom(double rightX, double rightY) throws RectangleException {
        checkAngles(this.leftX, this.leftY, rightX, rightY);
        this.rightX = rightX;
        this.rightY = rightY;
    }

    private static void checkAngles(double leftX, double leftY, double rightX, double rightY) throws RectangleException {
        if (leftX > rightX || leftY > rightY) {
            throw new RectangleException("Wrong coordinates");
        }
    }

    @Override
    public String toString() {
        return this.getLeftX() + " " + this.getLeftY() + " " + this.getRightX() + " " + this.getRightY() + " ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.leftX, leftX) == 0 &&
                Double.compare(rectangle.leftY, leftY) == 0 &&
                Double.compare(rectangle.rightX, rightX) == 0 &&
                Double.compare(rectangle.rightY, rightY) == 0;
    }
}