package task3_collections;

import task1_basics.trainee.Trainee;
import task3_collections.meta.MetaStringException;

import java.util.Arrays;
import java.util.Objects;

public class Group {
    private String title;
    private Trainee[] students;

    public Group(String title, Trainee... students) throws MetaStringException {
        setTitle(title);
        setStudents(students);
    }

    public Group(Group group) throws MetaStringException {
        setTitle(group.getTitle());
        setStudents(group.getStudents());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) throws MetaStringException {
        checkTitle(title);
        this.title = title;
    }

    public Trainee[] getStudents() {
        return students;
    }

    public void setStudents(Trainee... students) throws MetaStringException {
        checkStudentsArray(students);
        this.students = Arrays.copyOf(students, students.length);
    }

    private static void checkTitle(String title) throws MetaStringException {
        if (title == null || title.equals("")) throw new MetaStringException("Title can't be empty");
    }

    private static void checkStudentsArray(Trainee... students) throws MetaStringException {
        boolean allNull = true;
        if (students.length == 0) {
            throw new MetaStringException("Students array can not be empty");
        } else {
            for (Trainee t : students) {
                if (t != null) {
                    allNull = false;
                    break;
                }
            }
        }
        if (allNull) {
            throw new MetaStringException("No existing students in array");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(title, group.title) &&
                Arrays.equals(students, group.students);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(title);
        result = 31 * result + Arrays.hashCode(students);
        return result;
    }

    public Trainee findByName(String name) throws MetaStringException {
        Trainee res = null;
        for (Trainee t : students) {
            if (t.getFirstName().compareTo(name) == 0) {
                res = t;
                break;
            }
        }
        if (res == null) throw new MetaStringException("Student with this name not found!");
        return res;
    }
}
