package task3_collections;

import task1_basics.trainee.Trainee;

import java.util.*;

public class TraineeCollections {

    public static Trainee findMaxGrade(Collection<Trainee> trs) {
        int g = 0;
        Trainee res = null;
        for (Trainee t : trs) {
            if (t.getGrade() > g) {
                res = t;
                g = t.getGrade();
                if (g == 5) {
                    break;
                }
            }
        }
        return res;
    }

    public static Trainee findByName(Collection<Trainee> trs, String name) {
        Trainee res = null;
        for (Trainee t : trs) {
            if (t.getFirstName().compareTo(name) == 0) {
                res = t;
                break;
            }
        }
        if (res == null) throw new NoSuchElementException("No trainees with this name in that list!");
        return res;
    }

    public static boolean isQueueEmpty(Queue<Trainee> queue) {
        if (queue.peek() == null) {
            return true;
        }
        return false;
    }

    public static boolean findWithAnotherLastName(TreeSet<Trainee> trs, Trainee someone) {
        Trainee lower = trs.lower(someone);
        Trainee higher = trs.higher(someone);
        if (lower != null && lower.getFirstName().equals(someone.getFirstName())) {
            return true;
        }
        return higher != null && higher.getFirstName().equals(someone.getFirstName());
    }
}
