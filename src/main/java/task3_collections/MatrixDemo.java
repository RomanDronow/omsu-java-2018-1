package task3_collections;


import java.util.*;

public class MatrixDemo {
    public static Set<Integer> numberOfUniqueRows(int[][] matrix) {
        List<List<Integer>> listMatrix = new ArrayList<>();
        for (int i = 0; i < matrix[0].length; i++) {
            ArrayList<Integer> temp = new ArrayList<>();
            for (int j = 0; j < matrix.length; j++) {
                temp.add(matrix[i][j]);
            }
            listMatrix.add(temp);
        }
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < listMatrix.size(); i++) {
            for (int j = 0; j < listMatrix.size(); j++) {
                Set<Integer> temp1 = new HashSet<>(listMatrix.get(i));
                Set<Integer> temp2 = new HashSet<>(listMatrix.get(j));
                if (i != j && (temp1.equals(temp2))) {
                    result.add(i);
                }
            }
        }
        return new HashSet<>(result);
    }
}
