package task3_collections;

import task3_collections.meta.MetaStringException;

public class Institute {
    private String title;
    private String city;

    public Institute(String title, String city) throws MetaStringException {
        setTitle(title);
        setCity(city);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) throws MetaStringException {
        checkTitle(title);
        this.title = title;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws MetaStringException {
        checkCity(city);
        this.city = city;
    }

    private static void checkCity(String s) throws MetaStringException {
        if(s == null || s.equals("")) throw new MetaStringException("City can't have an empty name");
    }

    private static void checkTitle(String s) throws MetaStringException {
        if(s == null || s.equals("")) throw new MetaStringException("Title can't have an empty name");
    }

    @Override
    public String toString() {
        return title + " in " + city;
    }
}
