package task3_collections.meta;

public class MetaStringException extends Exception {
    public MetaStringException(String message) {
        super(message);
    }
}
