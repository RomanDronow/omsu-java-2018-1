package task0_v14;

import java.util.ArrayList;

public class SurfaceList {
    private ArrayList<Surface> surfaces;

    public SurfaceList() {
        surfaces = new ArrayList<>();
    }

    public void addSurface(Surface surface){
        surfaces.add(surface);
    }

    public void addSurface(String name, SurfaceType type) {
        surfaces.add(new Surface(name, type));
    }

    public Surface getSurface(int index){
        return surfaces.get(index);
    }

    public void removeSurface(int index){
        surfaces.remove(index);
    }

}
