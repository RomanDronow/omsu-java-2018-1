package task0_v14;

import java.util.Objects;

import static task0_v14.SurfaceType.*;

/*
                         Дронов Роман, задание V14
     Сформировать объект "список элементов поверхности земного шара
     и написать unit-тесты, элементы - либо объекты суши, либо
     водные объекты. Первые - либо равнины, либо горы, вторые -
                        пресноводные или соленые.
*/

public class Surface {
    private String name;
    private SurfaceType type;

    public Surface() {
        this.name = "Default";
        this.type = PLAIN;
    }

    public Surface(String name, SurfaceType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SurfaceType getType() {
        return type;
    }

    public void setType(SurfaceType type) {

        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Surface surface = (Surface) o;
        return Objects.equals(name, surface.name) &&
                type == surface.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, type);
    }

    @Override
    public String toString() {
        return type.toString().toLowerCase() + " " + name;
    }
}
