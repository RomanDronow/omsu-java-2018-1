package task5_java8features;

public class PersonMF {
    private PersonMF mother, father;

    public PersonMF(PersonMF mother, PersonMF father) {
        this.mother = mother;
        this.father = father;
    }

    public PersonMF getMother() {
        return mother;
    }

    public void setMother(PersonMF mother) {
        this.mother = mother;
    }

    public PersonMF getFather() {
        return father;
    }

    public void setFather(PersonMF father) {
        this.father = father;
    }

    public PersonMF getMotherMotherFather(){
        return getMother() != null ? getMother().getMother() != null ? getMother().getMother().getFather() : null : null;
    }
}
