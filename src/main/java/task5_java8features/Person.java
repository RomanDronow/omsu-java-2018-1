package task5_java8features;

public class Person {
    private String name;
    private int age;

    public Person() {
        name = "Person";
    }

    public Person(String name) {
        this.name = name;
        this.age = 5;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
