package task5_java8features;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Lambda {
    public static final Function<String, List<String>> split = string -> Arrays.asList(string.split(" "));

    public static final Function<List<?>, Integer> count = List::size;

    public static final MyFunction<String, Integer> splitAndCount = string -> split.andThen(count).apply(string);

    public static final MyFunction<String, Integer> splitAndCountCompose = string -> count.compose(split).apply(string);

    public static final MyFunction<String, Person> create = Person::new;

    public static final BiFunction<Integer, Integer, Integer> max = Math::max;

    public static final Supplier<Date> getCurrentDate = Date::new;

    public static final Predicate<Integer> isEven = i -> i % 2 == 0;

    public static final BiPredicate<Integer, Integer> areEqual = Integer::equals;

    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.parallel().map(op);
    }

    public static final Function<List<Person>, List<String>> uniqueNamesBelowThirty = list -> list != null ?
            Arrays.stream(list.stream().
                    filter(p -> p.getAge() > 30).
                    sorted(Comparator.comparingInt(p -> p.getName().length())).
                    map(Person::getName).
                    toArray(String[]::new)).distinct().
                    collect(Collectors.toList()) : null;

    public static final Function<List<Person>,List<String>> uniqueNamesBelowThirtyGrouped = list -> list != null ?
            new ArrayList<>(list.stream().filter(person -> person.getAge() > 30)
                    .collect(Collectors.groupingBy(Person::getName)).keySet()) : null;

    public static int sum(List<Integer> list){
        return list.stream().reduce((s1,s2) -> s1 + s2).orElse(0);
    }
    public static int product(List<Integer> list){
        return list.stream().reduce((s1,s2) -> s1 * s2).orElse(0);
    }

}
