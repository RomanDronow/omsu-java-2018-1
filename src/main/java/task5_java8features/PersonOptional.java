package task5_java8features;

import java.util.Optional;

public class PersonOptional {
    private Optional<PersonOptional> mother, father;

    public PersonOptional(PersonOptional mother, PersonOptional father){
        this.mother = Optional.ofNullable(mother);
        this.father = Optional.ofNullable(father);
    }

    public Optional<PersonOptional> getMother() {
        return mother;
    }

    public Optional<PersonOptional> getFather() {
        return father;
    }

    public PersonOptional getMotherMotherFather(){
        return mother.flatMap(p -> p.getMother()
                .flatMap(PersonOptional::getMother)
                .flatMap(PersonOptional::getFather))
                .orElse(null);
    }
}
