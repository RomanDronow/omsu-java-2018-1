package task4_multithreading.collections;

import task1_basics.trainee.Trainee;
import task3_collections.meta.MetaStringException;

import java.util.Arrays;

public class Group {
    private String title;
    private Trainee[] students;

    public Group(String title, Trainee... students) throws MetaStringException {
        checkTitle(title);
        checkStudentsArray(students);
        setTitle(title);
        setStudents(students);
    }

    public Group(Group group) throws MetaStringException {
        setTitle(group.getTitle());
        setStudents(group.getStudents());
    }

    public String getTitle() {

        synchronized (title) {
            return title;
        }
    }

    public void setTitle(String title) throws MetaStringException {
        checkTitle(title);
        synchronized (this.title) {
            this.title = title;
        }
    }

    public Trainee[] getStudents() {
        synchronized (students) {
            return students;
        }
    }

    public void setStudents(Trainee... students) throws MetaStringException {
        checkStudentsArray(students);
        synchronized (this.students) {
            this.students = Arrays.copyOf(students, students.length);
        }
    }

    private static void checkTitle(String title) throws MetaStringException {
        if (title == null || title.equals("")) throw new MetaStringException("Title can't be empty");
    }

    private static void checkStudentsArray(Trainee... students) throws MetaStringException {
        boolean allNull = true;
        if (students.length == 0) {
            throw new MetaStringException("Students array can not be empty");
        } else {
            for (Trainee t : students) {
                if (t != null) {
                    allNull = false;
                    break;
                }
            }
        }
        if (allNull) {
            throw new MetaStringException("No existing students in array");
        }
    }
}
