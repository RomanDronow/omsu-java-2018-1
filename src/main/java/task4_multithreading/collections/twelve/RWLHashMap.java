package task4_multithreading.collections.twelve;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RWLHashMap<K, V> {
    private Map<K, V> map;
    private Lock readLock;
    private Lock writeLock;

    public RWLHashMap() {
        map = new HashMap<>();
        ReadWriteLock locker = new ReentrantReadWriteLock();
        readLock = locker.readLock();
        writeLock = locker.writeLock();
    }

    public V get(K key) {
        readLock.lock();
        V result = map.get(key);
        readLock.unlock();
        return result;
    }

    public void put(K key, V value) {
        writeLock.lock();
        map.put(key, value);
        writeLock.unlock();
    }
}
