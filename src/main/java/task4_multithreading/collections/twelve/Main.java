package task4_multithreading.collections.twelve;

import java.util.Random;

public class Main {
    public static void concurrentHashMapThreads(){
        RWLHashMap<Integer, String> map = new RWLHashMap<>();
        Random rnd = new Random();

        Thread adder = new Thread(() -> {
            for(int i = 0; i < 100; i++)
                map.put(i, Integer.toString(i));
        });

        Thread reader = new Thread(() -> {
                for (int i = 0; i < 100; i++)
                    System.out.println("Ordered " + map.get(i));
        });

        Thread randomReader = new Thread(() -> {
            for(int i = 0; i < 100; i++)
                System.out.println("Random " + map.get(rnd.nextInt(100)));
        });
        adder.start();
        randomReader.start();
        reader.start();
    }

    public static void main(String[] args) {
        concurrentHashMapThreads();
    }
}
