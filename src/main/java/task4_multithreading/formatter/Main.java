package task4_multithreading.formatter;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Formatter formatter = new Formatter();
        /*FormatterThread formatterThread1 = new FormatterThread(formatter);
        FormatterThread formatterThread2 = new FormatterThread(formatter);
        FormatterThread formatterThread3 = new FormatterThread(formatter);
        FormatterThread formatterThread4 = new FormatterThread(formatter);
        FormatterThread formatterThread5 = new FormatterThread(formatter);
        formatterThread1.start();
        formatterThread2.start();
        formatterThread3.start();
        formatterThread4.start();
        formatterThread5.start();*/

        for (int i = 0; i < 5; i++) {
            new FormatterThread(formatter).start();
        }
    }
}
