package task4_multithreading.formatter;

import java.util.Date;

public class FormatterThread extends Thread {
    private Formatter formatter;

    public FormatterThread(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        System.out.println(formatter.format(new Date()));
    }
}
