package task4_multithreading.formatter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
    public String format(Date date){
        ThreadLocal<Date> localDate = new ThreadLocal<Date>();
        localDate.set(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        return dateFormat.format(localDate.get());
    }
}
