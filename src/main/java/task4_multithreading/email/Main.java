package task4_multithreading.email;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws IOException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        EmailContainer container = new EmailContainer("resources/address.txt");
        synchronized (container) {
            Transport transport = new Transport();
            for (int i = 0; i < 10; i++) {
                executor.submit(new TransportThread(container, transport));
            }
        }
        executor.shutdown();
        if (executor.isTerminated()) {
            container.close();
        }
    }
}
