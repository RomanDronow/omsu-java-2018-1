package task4_multithreading.email;

import java.io.FileNotFoundException;
import java.io.IOException;

public class TransportThread extends Thread {
    private EmailContainer container;
    private Transport transport;

    public TransportThread(EmailContainer container, Transport transport) {
        this.container = container;
        this.transport = transport;
    }

    @Override
    public void run() {
        String email = null;
        try {
            if(container.hasMore()) {
                email = container.getEmail();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            transport.send(new Message(email, "Roman Dronov", "Java", "Testing...\n"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
