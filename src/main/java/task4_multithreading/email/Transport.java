package task4_multithreading.email;

import java.io.*;

public class Transport {
    public void send(Message message) throws FileNotFoundException {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter("resources/out.txt", true))) {
            synchronized (bw) {
                bw.write(message.toString());
            }
            Thread.sleep(1000);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
