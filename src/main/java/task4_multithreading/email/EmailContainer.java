package task4_multithreading.email;

import java.io.*;

public class EmailContainer implements Closeable, AutoCloseable {
    private final BufferedReader reader;

    public EmailContainer(String fileName) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(fileName));
    }

    public String getEmail() throws IOException {
        synchronized (reader) {
            return reader.readLine();
        }
    }

    public boolean hasMore() throws IOException {
        synchronized (reader) {
            return reader.ready();
        }
    }

    @Override
    public void close() throws IOException {
        synchronized (reader) {
            reader.close();
        }
    }
}