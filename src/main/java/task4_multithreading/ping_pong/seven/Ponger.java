package task4_multithreading.ping_pong.seven;

public class Ponger extends Thread {
    private PingPong pingPong;

    public Ponger(PingPong pingPong) {
        this.pingPong = pingPong;
    }

    @Override
    public void run(){
        for (int i = 0; i < 1000; i++) {
            try{
                pingPong.pong();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

