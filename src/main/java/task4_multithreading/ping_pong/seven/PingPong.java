package task4_multithreading.ping_pong.seven;

import java.util.concurrent.Semaphore;

public class PingPong {
    private Semaphore ping;
    private Semaphore pong;

    public PingPong() {
        this.ping = new Semaphore(1);
        this.pong = new Semaphore(0);
    }

    public void ping() throws InterruptedException {
        ping.acquire();
        System.out.println("Ping");
        pong.release();
    }

    public void pong() throws InterruptedException {
        pong.acquire();
        System.out.println("Pong");
        ping.release();
    }
}
