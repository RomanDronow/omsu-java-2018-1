package task4_multithreading.ping_pong.seven;

public class Main {
    public static void main(String[] args) {
        PingPong pingPong = new PingPong();
        Pinger pinger = new Pinger(pingPong);
        Ponger ponger = new Ponger(pingPong);
        pinger.start();
        ponger.start();
    }
}
