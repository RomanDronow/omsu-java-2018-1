package task4_multithreading.ping_pong.seven;

public class Pinger extends Thread {
    private PingPong pingPong;

    public Pinger(PingPong pingPong) {
        this.pingPong = pingPong;
    }

    @Override
    public void run(){
        for (int i = 0; i < 1000; i++) {
            try{
                pingPong.ping();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
