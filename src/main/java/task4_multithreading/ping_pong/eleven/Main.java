package task4_multithreading.ping_pong.eleven;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        ConditionalPingPong pingPong = new ConditionalPingPong(lock,condition);
        ConditionalPinger pinger = new ConditionalPinger(pingPong);
        ConditionalPonger ponger = new ConditionalPonger(pingPong);
        pinger.start();
        ponger.start();
    }
}
