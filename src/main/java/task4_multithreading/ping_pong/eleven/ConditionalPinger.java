package task4_multithreading.ping_pong.eleven;

public class ConditionalPinger extends Thread {
    private ConditionalPingPong pingPong;

    public ConditionalPinger(ConditionalPingPong pingPong) {
        this.pingPong = pingPong;
    }

    @Override
    public void run() {
        while (true) {
            pingPong.ping();
        }
    }
}
