package task4_multithreading.ping_pong.eleven;

public class ConditionalPonger extends Thread {
    private ConditionalPingPong pingPong;

    public ConditionalPonger(ConditionalPingPong pingPong) {
        this.pingPong = pingPong;
    }

    @Override
    public void run() {
        while (true) {
            pingPong.pong();
        }
    }
}

