package task4_multithreading.ping_pong.eleven;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class ConditionalPingPong {
    private Lock lock;
    private Condition condition;
    private boolean ping = true;
    private boolean falseStart = false;
    private boolean pong = true;

    public ConditionalPingPong(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    public void ping() {
        lock.lock();
        if (!ping) {
            try {
                condition.await();
            } catch (InterruptedException e) {
            }
            ping = true;
        }
        System.out.println("Ping");
        ping = false;
        if (!falseStart) {
            falseStart = true;
        }
        condition.signalAll();
    }

    public void pong() {
        while (falseStart) {
            lock.lock();
            if (!pong) {
                try {
                    condition.await();
                } catch (InterruptedException e) {
                }
                pong = true;
            }
            System.out.println("Pong");
            pong = false;
            condition.signalAll();
        }
    }
}
