package task4_multithreading.data;

import java.util.Arrays;

public class ReaderThread extends Thread {
    private DataQueue dataQueue;

    public ReaderThread(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println(Arrays.toString(dataQueue.get().getData()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
