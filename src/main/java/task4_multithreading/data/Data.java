package task4_multithreading.data;

public class Data {
    private int[] data;

    public Data(int[] data) {
        this.data = data;
    }

    public int[] getData(){
        return data;
    }
}
