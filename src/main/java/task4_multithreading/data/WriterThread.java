package task4_multithreading.data;

import java.util.Random;

public class WriterThread extends Thread {
    private DataQueue dataQueue;

    public WriterThread(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        while (true) {
            Random random = new Random();
            int size = random.nextInt(10);
            if (size == 0) {
                size++;
            }
            int[] data = new int[size];
            for (int i = 0; i < size; i++) {
                data[i] = random.nextInt(10);
            }
            if (dataQueue.getCapacity() != 0) {
                dataQueue.add(new Data(data));
            }
        }
    }
}
