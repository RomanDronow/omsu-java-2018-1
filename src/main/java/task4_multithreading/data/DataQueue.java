package task4_multithreading.data;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DataQueue {
    private BlockingQueue<Data> datas;

    public DataQueue(int capacity){
        datas = new LinkedBlockingQueue<>(capacity);
    }

    public void add(Data d){
        datas.add(d);
    }

    public Data get() throws InterruptedException {
        return datas.take();
    }

    public int getCapacity(){
        return datas.remainingCapacity();
    }
}
