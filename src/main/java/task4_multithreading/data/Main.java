package task4_multithreading.data;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DataQueue dataQueue = new DataQueue(20);
        int readersCount = scanner.nextInt();
        int writersCount = scanner.nextInt();
        for (int i = 0; i < readersCount; i++) {
            new ReaderThread(dataQueue).start();
        }
        for (int i = 0; i < writersCount; i++) {
            new WriterThread(dataQueue).start();
        }
    }
}
