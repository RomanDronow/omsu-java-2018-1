package task4_multithreading.executables;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TaskQueue taskQueue = new TaskQueue(20);
        int executorsCount = scanner.nextInt();
        int developersCount = scanner.nextInt();
        for (int i = 0; i < executorsCount; i++) {
            new ExecutorThread(taskQueue).start();
        }
        for (int i = 0; i < developersCount; i++) {
            new DeveloperThread(taskQueue).start();
        }
    }
}
