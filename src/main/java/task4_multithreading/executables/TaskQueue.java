package task4_multithreading.executables;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class TaskQueue {
    private BlockingQueue<Task> taskQueue;

    public TaskQueue(int capacity){
        taskQueue = new LinkedBlockingDeque<>();
    }

    public void add(Task t){
        taskQueue.add(t);
    }

    public Task get() throws InterruptedException {
        return taskQueue.take();
    }

    public  int getCapacity(){
        return taskQueue.remainingCapacity();
    }
}
