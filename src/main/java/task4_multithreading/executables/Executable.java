package task4_multithreading.executables;

public interface Executable {
    void execute();
}
