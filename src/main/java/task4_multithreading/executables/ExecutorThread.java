package task4_multithreading.executables;

public class ExecutorThread extends Thread{
    private TaskQueue taskQueue;

    public ExecutorThread(TaskQueue taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while(true){
            try {
                taskQueue.get().execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
