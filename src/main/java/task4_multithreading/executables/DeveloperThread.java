package task4_multithreading.executables;

public class DeveloperThread extends Thread {
    private TaskQueue taskQueue;

    public DeveloperThread(TaskQueue taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while (true) {
            if (taskQueue.getCapacity() > 0) {
                taskQueue.add(new Task());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
