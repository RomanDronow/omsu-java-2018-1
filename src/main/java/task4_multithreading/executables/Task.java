package task4_multithreading.executables;

public class Task implements Executable {
    @Override
    public void execute() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Task is done");
    }
}
