package task4_multithreading.arraylist.five;

import java.util.List;
import java.util.Random;

public class Controller {
    private List<Integer> list;

    public Controller(List<Integer> list) {
        this.list = list;
    }

    public synchronized void add() {
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            list.add(rand.nextInt(1000));
        }
    }

    public synchronized void remove() {
        System.out.println();
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            if (!list.isEmpty()) {
                int pos = rand.nextInt(list.size());
                if (pos == list.size()) {
                    pos--;
                    list.remove(pos);
                }
            }
        }
    }
}
