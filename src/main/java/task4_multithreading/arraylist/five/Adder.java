package task4_multithreading.arraylist.five;

public class Adder extends Thread{
    private Controller controller;

    public Adder(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void run() {
        controller.add();
    }
}
