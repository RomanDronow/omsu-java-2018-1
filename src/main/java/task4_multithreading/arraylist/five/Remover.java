package task4_multithreading.arraylist.five;

public class Remover extends Thread{
    private Controller controller;

    public Remover(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void run() {
        controller.remove();
    }
}
