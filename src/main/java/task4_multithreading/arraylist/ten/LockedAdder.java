package task4_multithreading.arraylist.ten;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockedAdder extends Thread {
    private final List<Integer> list;
    private Lock lock;

    public LockedAdder(List<Integer> list) {
        this.list = list;
        this.lock = new ReentrantLock();
    }

    public void add() {
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
                lock.lock();
                list.add(rand.nextInt(1000));
                lock.unlock();
        }
    }

    @Override
    public void run() {
        add();
    }
}
