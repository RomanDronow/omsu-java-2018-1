package task4_multithreading.arraylist.ten;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockedRemover extends Thread {
    private final List<Integer> list;
    private Lock lock;

    public LockedRemover(List<Integer> list) {
        this.list = list;
        this.lock = new ReentrantLock();
    }

    public void remove() {
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            lock.lock();
            if (!list.isEmpty()) {
                int pos = rand.nextInt(list.size());
                if (pos == list.size()) {
                    pos--;
                }
                list.remove(pos);
            }
            lock.unlock();
        }
    }

    @Override
    public void run() {
        remove();
    }
}
