package task4_multithreading.arraylist.ten;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        LockedAdder lockedAdder = new LockedAdder(list);
        LockedRemover lockedRemover = new LockedRemover(list);
        lockedAdder.start();
        lockedRemover.start();
    }
}
