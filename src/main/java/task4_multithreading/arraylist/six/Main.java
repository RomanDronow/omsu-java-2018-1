package task4_multithreading.arraylist.six;

import task4_multithreading.arraylist.five.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        Controller controller = new Controller(list);
        Adder adder = new Adder(controller);
        Remover remover = new Remover(controller);

        adder.start();
        remover.start();
    }
}
