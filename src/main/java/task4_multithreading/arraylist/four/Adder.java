package task4_multithreading.arraylist.four;

import java.util.List;
import java.util.Random;

public class Adder extends Thread {
    private final List<Integer> list;

    public Adder(List<Integer> list) {
        this.list = list;
    }

    public void add() {
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            synchronized (list) {
                list.add(rand.nextInt(1000));
            }
        }
    }

    @Override
    public void run() {
        add();
    }
}
