package task4_multithreading.arraylist.four;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Adder adder = new Adder(list);
        Remover remover = new Remover(list);
        adder.start();
        remover.start();
    }
}
