package task4_multithreading.arraylist.four;

import java.util.List;
import java.util.Random;

public class Remover extends Thread{
    private final List<Integer> list;

    public Remover(List<Integer> list) {
        this.list = list;
    }

    public void remove() {
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            synchronized (list) {
                if (!list.isEmpty()) {
                    int pos = rand.nextInt(list.size());
                    if (pos == list.size()) {
                        pos--;
                    }
                    list.remove(pos);
                }
            }
        }
    }

    @Override
    public void run() {
        remove();
    }
}
