package task4_multithreading.sample_threads.two;

public class SampleThread  implements Runnable{
    String threadName;

    public SampleThread(String threadName) {
        this.threadName = threadName;
    }

    @Override
    public void run() {
        System.out.printf("\nThread name: %s", threadName);
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.printf("Thread %s interrupted", threadName);
        }
        System.out.printf("\nThread %s finished", threadName);
    }
}
