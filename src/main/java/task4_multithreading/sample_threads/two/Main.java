package task4_multithreading.sample_threads.two;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main thread started");
        SampleThread sampleThread = new SampleThread("Phoenix");
        Thread thread = new Thread(sampleThread);
        thread.start();
        thread.join();
        System.out.println("\nMain thread finished");
    }
}
