package task4_multithreading.sample_threads.one;

public class Main {
    public static String getThreadProperties(){
        return Thread.currentThread().toString();
    } // Возвращает свойства текущего потока в виде [название, приоритет, группа]

    public static void main(String[] args) {
        System.out.println(getThreadProperties());
    }
}
