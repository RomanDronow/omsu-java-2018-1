package task4_multithreading.sample_threads.three;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main thread started");
        SampleThread1 sampleThread1 = new SampleThread1("Sample #1");
        SampleThread2 sampleThread2 = new SampleThread2("Sample #2");
        SampleThread3 sampleThread3 = new SampleThread3("Sample #3");

        Thread thread1 = new Thread(sampleThread1);
        Thread thread2 = new Thread(sampleThread2);
        Thread thread3 = new Thread(sampleThread3);

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        System.out.println("\nMain thread finished");

    }
}

