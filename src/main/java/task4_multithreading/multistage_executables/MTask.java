package task4_multithreading.multistage_executables;

import java.util.LinkedList;
import java.util.List;

public class MTask {
    private LinkedList<Executable> stages;
    private String name;
    private int stagesCount;

    public MTask(String name, List<Task> stages) {
        this.stages = new LinkedList<>(stages);
        this.name = name;
        this.stagesCount = stages.size();
    }

    public String getName() {
        return name;
    }

    public synchronized boolean hasStage(){
        return stages.size() > 0;
    }

    public Executable getStage(){
        try{
            synchronized (stages){
                return stages.pollFirst();
            }
        } catch (IndexOutOfBoundsException e){
            return null;
        }
    }

    @Override
    public String toString() {
        return name + " " + stages.size() + "/" + stagesCount;
    }

    public synchronized void setPoison(){
        stages.add(new Poison());
    }

    public synchronized LinkedList<Executable> getStages(){
        return stages;
    }

    public synchronized void setStages(LinkedList<Executable> list){
        this.stages = list;
    }
}
