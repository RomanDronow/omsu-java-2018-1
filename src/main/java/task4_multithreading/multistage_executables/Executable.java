package task4_multithreading.multistage_executables;

public interface Executable {
    void execute();
}
